let expect = require("chai").expect;
let should = require('should');
let assert = require('assert');

let Maze = require('../src/model/maze').model.Maze;


describe("Maze", function () {

    before(function () {
        // runs before all tests in this block
    });

    after(function () {
        // runs after all tests in this block
    });

    beforeEach(function () {
        // runs before each test in this block
    });

    afterEach(function () {
        // runs after each test in this block
    });

    describe('Validations', function () {
        it('should be an object', function () {

            let maze = new Maze([],[]);

            assert(maze!=undefined);
            assert.equal(1,1);
        });
        it('should have a property cells when passed in constructor', function () {
            let maze = new Maze([]);
            maze.should.have.property("cells");
        });
    });
    describe('#checkIfIsValid', function () {
        it('Valid maze, case 1', function () {
            let maze = new Maze([
                [0, 0, 0, 0, 0, 0, 0],
                [0, 1, 1, 1, 1, 1, 0],
                [0, 1, 2, 1, 2, 1, 0],
                [0, 1, 1, 1, 1, 1, 0],
                [0, 0, 0, 0, 0, 0, 0]
            ],
                [0, 2]
            );
            assert.equal(maze.checkIfIsValid().isValid, true);
        });
        it('Valid maze, case 2', function () {
            let maze = new Maze([
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 1, 1, 1, 1, 0, 0],
                    [0, 1, 0, 0, 1, 0, 0],
                    [0, 1, 1, 1, 1, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0]
                ],
                [0, 2]
            );
            assert.equal(maze.checkIfIsValid().isValid, true);
        });
        it('Invalid maze, case 1', function () {
            let maze = new Maze([
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 0, 0, 1, 0, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 0, 0, 0, 0, 0, 0]
                ],
                [0, 2]
            );
            assert.equal(maze.checkIfIsValid().isValid, false);
        });
        it('Invalid maze, case 2', function () {
            let maze = new Maze([
                    [0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0],
                    [0, 0, 1, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0]
                ],
                [0, 2]
            );
            assert.equal(maze.checkIfIsValid().isValid, false);
        });
        it('Invalid maze, case 3', function () {
            let maze = new Maze([
                    [0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0]
                ],
                [0, 2]
            );
            assert.equal(maze.checkIfIsValid().isValid, false);
        });
        it('Invalid maze, case 4', function () {
            let maze = new Maze([
                    [0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0],
                    [0, 1, 1, 1, 1, 0],
                    [0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0]
                ],
                [0, 2]
            );
            assert.equal(maze.checkIfIsValid().isValid, false);
        });
        it('Invalid maze, case 5', function () {
            let maze = new Maze([
                    [0, 0, 0, 0, 0, 0],
                    [1, 1, 1, 1, 1, 0],
                    [0, 1, 0, 0, 1, 0],
                    [0, 1, 1, 1, 1, 0],
                    [0, 0, 0, 0, 0, 0]
                ],
                [0, 2]
            );
            assert.equal(maze.checkIfIsValid().isValid, false);
        });
        it('Invalid maze, case 6', function () {
            let maze = new Maze([
                    [0, 0, 0, 0, 0, 0],
                    [0, 1, 1, 1, 1, 0],
                    [0, 1, 1, 0, 1, 0],
                    [0, 1, 1, 1, 1, 0],
                    [0, 0, 0, 0, 0, 0]
                ],
                [0, 2]
            );
            assert.equal(maze.checkIfIsValid().isValid, false);
        });
        it('Invalid maze, case 7', function () {
            let maze = new Maze([
                    [0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 1, 1, 1, 0, 1, 1, 1, 0],
                    [0, 1, 0, 1, 0, 1, 0, 1, 0],
                    [0, 1, 1, 1, 0, 1, 1, 1, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0]
                ],
                [0, 2]
            );
            assert.equal(maze.checkIfIsValid().isValid, false);
        });
    });
});


