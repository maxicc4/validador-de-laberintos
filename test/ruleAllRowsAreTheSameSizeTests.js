let assert = require('assert');

let RuleAllRowsAreTheSameSize = require('../src/model/ruleAllRowsAreTheSameSize').model.RuleAllRowsAreTheSameSize;
let CellPassable = require('../src/model/cellPassable').model.CellPassable;

describe('RuleAllRowsAreTheSameSize', function() {
	let rule = new RuleAllRowsAreTheSameSize();
	
	describe('#isMet()', function() {
        it('should return true when passed only 1 row', function() {
        	assert.equal(rule.isMet([[new CellPassable(),new CellPassable(),new CellPassable()]]), true);
        });
        it('should return true when passed 2 rows of the same size', function() {
            assert.equal(rule.isMet([
            	[new CellPassable(),new CellPassable(),new CellPassable()],
                [new CellPassable(),new CellPassable(),new CellPassable()],
			]), true);
        });
        it('should return false when passed 2 rows of different size', function() {
            assert.equal(rule.isMet([
                [new CellPassable(),new CellPassable(),new CellPassable()],
                [new CellPassable(),new CellPassable()],
            ]), false);
        });
        it('should return false when passed 3 rows of different size (2 equals)', function() {
            assert.equal(rule.isMet([
                [new CellPassable(),new CellPassable()],
                [new CellPassable(),new CellPassable()],
                [new CellPassable(),new CellPassable(),new CellPassable()]
            ]), false);
        });
        it('should return true when passed 3 rows of the same size', function() {
            assert.equal(rule.isMet([
                [new CellPassable(),new CellPassable()],
                [new CellPassable(),new CellPassable()],
                [new CellPassable(),new CellPassable()],
            ]), true);
        });
	});
});
