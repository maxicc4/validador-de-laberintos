let assert = require('assert');

let RuleAllPassableCellsMustBeConnectedWithAtLeastTwoCellsPassable = require('../src/model/ruleAllPassableCellsMustBeConnectedWithAtLeastTwoCellsPassable').model.RuleAllPassableCellsMustBeConnectedWithAtLeastTwoCellsPassable;
let CellsContainer = require('../src/model/cellsContainer').model.CellsContainer;
let CellPassable = require('../src/model/cellPassable').model.CellPassable;
let CellImpassable = require('../src/model/cellImpassable').model.CellImpassable;

describe('RuleAllPassableCellsMustBeConnectedWithAtLeastTwoCellsPassable', function() {
	let rule = new RuleAllPassableCellsMustBeConnectedWithAtLeastTwoCellsPassable();
	
	describe('#isMet()', function() {
        it('should return false when passed only 1 row with all cells passables', function() {
        	assert.equal(rule.isMet(new CellsContainer(
        	    [new CellPassable(),new CellPassable(),new CellPassable()]
            )), false);
        });
        it('should return true when passed 2 rows and 3 cols with all cells passables', function() {
            assert.equal(rule.isMet(new CellsContainer(
                [new CellPassable(),new CellPassable(),new CellPassable()],
                [new CellPassable(),new CellPassable(),new CellPassable()]
            )), true);
        });
        it('should return false when passed 3 rows and 3 cols with only 1 cell passable', function() {
            assert.equal(rule.isMet(new CellsContainer(
                [new CellImpassable(),new CellImpassable(),new CellImpassable()],
                [new CellImpassable(),new CellPassable(),new CellImpassable()],
                [new CellImpassable(),new CellImpassable(),new CellImpassable()]
            )), false);
        });
        it('should return true when passed 3 rows and 3 cols with all cells impassable', function() {
            assert.equal(rule.isMet(new CellsContainer(
                [new CellImpassable(),new CellImpassable(),new CellImpassable()],
                [new CellImpassable(),new CellImpassable(),new CellImpassable()],
                [new CellImpassable(),new CellImpassable(),new CellImpassable()]
            )), true);
        });
        it('should return true when passed 3 rows and 3 cols with all edge cells impassable', function() {
            assert.equal(rule.isMet(new CellsContainer(
                [new CellPassable(),new CellPassable(),new CellPassable()],
                [new CellPassable(),new CellImpassable(),new CellPassable()],
                [new CellPassable(),new CellPassable(),new CellPassable()]
            )), true);
        });
        it('should return false when passed 3 rows and 3 cols with all edge cells impassable except one', function() {
            assert.equal(rule.isMet(new CellsContainer(
                [new CellPassable(),new CellPassable(),new CellPassable()],
                [new CellPassable(),new CellImpassable(),new CellPassable()],
                [new CellPassable(),new CellPassable(),new CellImpassable()]
            )), false);
        });
	});
});
