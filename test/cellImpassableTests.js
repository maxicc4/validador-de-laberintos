let assert = require('assert');

let CellImpassable = require('../src/model/cellImpassable').model.CellImpassable;

describe('CellImpassable', function() {
	describe('#isPassable()', function() {
		it('should return false', function() {
			let cell = new CellImpassable();
			assert.equal(cell.isPassable(), false);
		});
	});
});
