let assert = require('assert');

let RuleThereCannotBeFourCellsAttached = require('../src/model/ruleThereCannotBeFourCellsAttached').model.RuleThereCannotBeFourCellsAttached;
let CellsContainer = require('../src/model/cellsContainer').model.CellsContainer;
let CellPassable = require('../src/model/cellPassable').model.CellPassable;
let CellImpassable = require('../src/model/cellImpassable').model.CellImpassable;

describe('RuleThereCannotBeFourCellsAttached', function() {
	let rule = new RuleThereCannotBeFourCellsAttached();
	
	describe('#isMet()', function() {
        it('should return true when passed [[p,p,p],[p,imp,p]]', function() {
        	assert.equal(rule.isMet(new CellsContainer(
        	    [new CellPassable(0,0),new CellPassable(0,1),new CellPassable(0,2)],
                [new CellPassable(1,0),new CellImpassable(1,1),new CellPassable(1,2)]
            )), true);
        });
        it('should return false when passed [[p,p,p],[p,p,p]]', function() {
            assert.equal(rule.isMet(new CellsContainer(
                [new CellPassable(0,0),new CellPassable(0,1),new CellPassable(0,2)],
                [new CellPassable(1,0),new CellPassable(1,1),new CellPassable(1,2)]
            )), false);
        });
        it('should return true when passed [[p,p,p],[p,imp,p],[p,p,p]]', function() {
            assert.equal(rule.isMet(new CellsContainer(
                [new CellPassable(0,0),new CellPassable(0,1),new CellPassable(0,2)],
                [new CellPassable(1,0),new CellImpassable(1,1),new CellPassable(1,2)],
                [new CellPassable(2,0),new CellPassable(2,1),new CellPassable(2,2)]
            )), true);
        });
        it('should return false when passed [[p,p,imp],[p,p,imp],[imp,imp,imp]]', function() {
            assert.equal(rule.isMet(new CellsContainer(
                [new CellPassable(0,0),new CellPassable(0,1),new CellImpassable(0,2)],
                [new CellPassable(1,0),new CellPassable(1,1),new CellImpassable(1,2)],
                [new CellImpassable(2,0),new CellImpassable(2,1),new CellImpassable(2,2)]
            )), false);
        });
	});
});
