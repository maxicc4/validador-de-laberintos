let assert = require('assert');

let RuleAtLeastTwoPassableCells = require('../src/model/ruleAtLeastTwoPassableCells').model.RuleAtLeastTwoPassableCells;
let CellPassable = require('../src/model/cellPassable').model.CellPassable;
let CellImpassable = require('../src/model/cellImpassable').model.CellImpassable;

describe('RuleAtLeastTwoPassableCells', function() {
	let rule = new RuleAtLeastTwoPassableCells();
	
	describe('#isMet()', function() {
		it('should return false when the cells is not passed', function() {
			assert.equal(rule.isMet(), false);
		});
        it('should return true when passed 1 row with 2 cells passables', function() {
        	assert.equal(rule.isMet([[new CellPassable(),new CellPassable()]]), true);
        });
        it('should return false when passed 1 row with 1 cell passable and 1 cell impassable', function() {
            assert.equal(rule.isMet([[new CellPassable(),new CellImpassable()]]), false);
        });
        it('should return false when passed 1 row with 1 cell passable and 2 cells impassables', function() {
            assert.equal(rule.isMet([[new CellPassable(),new CellImpassable(),new CellImpassable()]]), false);
        });
        it('should return true when passed 1 row with 2 cells passables and 1 cell impassable', function() {
            assert.equal(rule.isMet([[new CellPassable(),new CellImpassable(),new CellPassable()]]), true);
        });
        it('should return false when passed 2 rows with 1 cell passable and 3 cells impassables', function() {
            assert.equal(rule.isMet([
            	[new CellPassable(),new CellImpassable()],
                [new CellImpassable(),new CellImpassable()]
			]), false);
        });
        it('should return true when passed 2 rows with 1 cell passable and 3 cells impassables', function() {
            assert.equal(rule.isMet([
                [new CellPassable(),new CellImpassable()],
                [new CellImpassable(),new CellPassable()]
            ]), true);
        });
	});
});
