let assert = require('assert');

let CellsContainer = require('../src/model/cellsContainer').model.CellsContainer;
let CellPassable = require('../src/model/cellPassable').model.CellPassable;
let CellImpassable = require('../src/model/cellImpassable').model.CellImpassable;

describe('CellsContainer', function() {
	describe('#constructor()', function() {
		it('should be equal to array with equal elements', function() {
			let cellsContainer = new CellsContainer(1,2,3);
            let array = [1,2,3];
			assert.equal(cellsContainer.length, array.length);
            assert.equal(cellsContainer[0], array[0]);
            assert.equal(cellsContainer[1], array[1]);
            assert.equal(cellsContainer[2], array[2]);
		});
	});
    describe('#getCellsPassablesConnectedWith()', function() {
        it('should return 2 cells when is passed position 0,0 with 2 rows and 2 cols with all cells passables', function() {
            let cellsContainer = new CellsContainer(
            	[new CellPassable(),new CellPassable()],
                [new CellPassable(),new CellPassable()]
			);
            assert.equal(cellsContainer.getCellsPassablesConnectedWith(0,0).length, 2);
        });
        it('should return 1 cell when is passed position 2,2 with 3 rows and 3 cols (connected only 1 cell passable)', function() {
            let cellsContainer = new CellsContainer(
                [new CellPassable(),new CellPassable(),new CellPassable()],
                [new CellPassable(),new CellPassable(),new CellImpassable()],
                [new CellPassable(),new CellPassable(),new CellPassable()]
            );
            assert.equal(cellsContainer.getCellsPassablesConnectedWith(2,2).length, 1);
        });
        it('should return 3 cells when is passed position 1,1 with 3 rows and 3 cols (connected 3 cells passable)', function() {
            let cellsContainer = new CellsContainer(
                [new CellPassable(),new CellPassable(),new CellPassable()],
                [new CellPassable(),new CellPassable(),new CellImpassable()],
                [new CellPassable(),new CellPassable(),new CellPassable()]
            );
            assert.equal(cellsContainer.getCellsPassablesConnectedWith(1,1).length, 3);
        });
        it('should return 4 cells when is passed position 1,1 with 3 rows and 3 cols (connected 4 cells passable)', function() {
            let cellsContainer = new CellsContainer(
                [new CellPassable(),new CellPassable(),new CellPassable()],
                [new CellPassable(),new CellImpassable(),new CellPassable()],
                [new CellPassable(),new CellPassable(),new CellPassable()]
            );
            assert.equal(cellsContainer.getCellsPassablesConnectedWith(1,1).length, 4);
        });
    });
    describe('#getAllCellsPassables()', function() {
        it('should return 4 cells when 2 rows and 2 cols with all cells passables', function() {
            let cellsContainer = new CellsContainer(
                [new CellPassable(),new CellPassable()],
                [new CellPassable(),new CellPassable()]
            );
            assert.equal(cellsContainer.getAllCellsPassables().length, 4);
        });
        it('should return 0 cells when 2 rows and 2 cols with all cells impassables', function() {
            let cellsContainer = new CellsContainer(
                [new CellImpassable(),new CellImpassable()],
                [new CellImpassable(),new CellImpassable()]
            );
            assert.equal(cellsContainer.getAllCellsPassables().length, 0);
        });
        it('should return 2 cells when 2 rows and 2 cols with 2 cells passables and 2 impassables', function() {
            let cellsContainer = new CellsContainer(
                [new CellImpassable(),new CellPassable()],
                [new CellPassable(),new CellImpassable()]
            );
            assert.equal(cellsContainer.getAllCellsPassables().length, 2);
        });
    });
});
