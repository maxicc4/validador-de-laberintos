let assert = require('assert');

let RuleImpassableEdges = require('../src/model/ruleImpassableEdges').model.RuleImpassableEdges;
let CellPassable = require('../src/model/cellPassable').model.CellPassable;
let CellImpassable = require('../src/model/cellImpassable').model.CellImpassable;

describe('RuleImpassableEdges', function() {
	let rule = new RuleImpassableEdges();
	
	describe('#isMet()', function() {
        it('should return false when passed only 1 row with all cells passables', function() {
        	assert.equal(rule.isMet([[new CellPassable(),new CellPassable(),new CellPassable()]]), false);
        });
        it('should return true when passed only 1 row with all cells impassables', function() {
            assert.equal(rule.isMet([[new CellImpassable(),new CellImpassable(),new CellImpassable()]]), true);
        });
        it('should return true when passed 2 rows with all cells impassables', function() {
            assert.equal(rule.isMet([
                [new CellImpassable(),new CellImpassable(),new CellImpassable()],
                [new CellImpassable(),new CellImpassable(),new CellImpassable()]
            ]), true);
        });
        it('should return false when passed 2 rows with all cells impassables and 1 passable', function() {
            assert.equal(rule.isMet([
                [new CellImpassable(),new CellImpassable(),new CellImpassable()],
                [new CellImpassable(),new CellImpassable(),new CellPassable()]
            ]), false);
        });
        it('should return true when passed 3 rows with edges impassables', function() {
            assert.equal(rule.isMet([
                [new CellImpassable(),new CellImpassable(),new CellImpassable()],
                [new CellImpassable(),new CellPassable(),new CellImpassable()],
                [new CellImpassable(),new CellImpassable(),new CellImpassable()]
            ]), true);
        });
        it('should return false when passed 3 rows with 1 cell the left edge passable', function() {
            assert.equal(rule.isMet([
                [new CellImpassable(),new CellImpassable(),new CellImpassable()],
                [new CellPassable(),new CellPassable(),new CellImpassable()],
                [new CellImpassable(),new CellImpassable(),new CellImpassable()]
            ]), false);
        });
	});
});
