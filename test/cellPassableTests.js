let assert = require('assert');

let CellPassable = require('../src/model/cellPassable').model.CellPassable;

describe('CellPassable', function() {
	describe('#isPassable()', function() {
		it('should return true', function() {
			let cell = new CellPassable();
			assert.equal(cell.isPassable(), true);
		});
	});
});
