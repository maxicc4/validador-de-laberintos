let assert = require('assert');

let RuleAllCellsAreConnected = require('../src/model/ruleAllCellsAreConnected').model.RuleAllCellsAreConnected;
let CellsContainer = require('../src/model/cellsContainer').model.CellsContainer;
let CellPassable = require('../src/model/cellPassable').model.CellPassable;
let CellImpassable = require('../src/model/cellImpassable').model.CellImpassable;

describe('RuleAllCellsAreConnected', function() {
	let rule = new RuleAllCellsAreConnected();
	
	describe('#isMet()', function() {
        it('should return true when passed only 1 row with all cells passables', function() {
        	assert.equal(rule.isMet(new CellsContainer(
        	    [new CellPassable(0,0),new CellPassable(0,1),new CellPassable(0,2)]
            )), true);
        });
        it('should return false when passed only 1 row (1 cell passable, 1 impassable, 1 passable)', function() {
            assert.equal(rule.isMet(new CellsContainer(
                [new CellPassable(0,0),new CellImpassable(0,1),new CellPassable(0,2)]
            )), false);
        });
        it('should return true when passed only 1 row (1 cell impassable, 1 passable, 1 passable)', function() {
            assert.equal(rule.isMet(new CellsContainer(
                [new CellImpassable(0,0),new CellPassable(0,1),new CellPassable(0,2)]
            )), true);
        });
        it('should return false when passed 2 rows ([p, imp, p],[imp, p, p])', function() {
            assert.equal(rule.isMet(new CellsContainer(
                [new CellPassable(0,0),new CellImpassable(0,1),new CellPassable(0,2)],
                [new CellImpassable(1,0),new CellPassable(1,1),new CellPassable(1,2)]
            )), false);
        });
        it('should return true when passed 2 rows ([p, imp, p],[p, p, p])', function() {
            assert.equal(rule.isMet(new CellsContainer(
                [new CellPassable(0,0),new CellImpassable(0,1),new CellPassable(0,2)],
                [new CellPassable(1,0),new CellPassable(1,1),new CellPassable(1,2)]
            )), true);
        });
        it('should return false when passed 3 rows ([p,p,p,imp,p,p,p],[p,imp,p,imp,p,imp,p],[p,p,p,imp,p,p,p])', function() {
            assert.equal(rule.isMet(new CellsContainer(
                [new CellPassable(0,0),new CellPassable(0,1),new CellPassable(0,2),new CellImpassable(0,3),new CellPassable(0,4),new CellPassable(0,5),new CellPassable(0,6)],
                [new CellPassable(1,0),new CellImpassable(1,1),new CellPassable(1,2),new CellImpassable(1,3),new CellPassable(1,4),new CellImpassable(1,5),new CellPassable(1,6)],
                [new CellPassable(2,0),new CellPassable(2,1),new CellPassable(2,2),new CellImpassable(2,3),new CellPassable(2,4),new CellPassable(2,5),new CellPassable(2,6)]
            )), false);
        });
        it('should return true when passed 3 rows ([p,p,p,imp,p,p,p],[p,imp,p,p,p,imp,p],[p,p,p,imp,p,p,p])', function() {
            assert.equal(rule.isMet(new CellsContainer(
                [new CellPassable(0,0),new CellPassable(0,1),new CellPassable(0,2),new CellImpassable(0,3),new CellPassable(0,4),new CellPassable(0,5),new CellPassable(0,6)],
                [new CellPassable(1,0),new CellImpassable(1,1),new CellPassable(1,2),new CellPassable(1,3),new CellPassable(1,4),new CellImpassable(1,5),new CellPassable(1,6)],
                [new CellPassable(2,0),new CellPassable(2,1),new CellPassable(2,2),new CellImpassable(2,3),new CellPassable(2,4),new CellPassable(2,5),new CellPassable(2,6)]
            )), true);
        });
	});
});
