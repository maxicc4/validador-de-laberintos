let Rule = require('./rule').model.Rule;

class RuleAllCellsAreConnected extends Rule {

    isMet(cells) {
    	if (cells !== undefined) {
    	    let cellsPassables = cells.getAllCellsPassables();
    	    if (cellsPassables.length > 0) {
    	        // Apartir de la primera celda transitable, voy llegando al resto de las celdas
    	        let cellCurrent = cellsPassables.shift();
    	        let cellsConnected = cells.getCellsPassablesConnectedWith(cellCurrent.row,cellCurrent.col);
    	        let indexOf = -1;

    	        while (cellsConnected.length > 0) {
    	            cellCurrent = cellsConnected.shift();
    	            indexOf = cellsPassables.indexOf(cellCurrent);
                    if (indexOf !== -1) {
                        cellsPassables.splice(indexOf, 1);
                        cellsConnected = cellsConnected.concat(cells.getCellsPassablesConnectedWith(cellCurrent.row,cellCurrent.col));
                    }
                }
            }
            return (cellsPassables.length <= 0);
    	}
    	return false;
    }
}

module.exports.model = {};
module.exports.model.RuleAllCellsAreConnected = RuleAllCellsAreConnected;
