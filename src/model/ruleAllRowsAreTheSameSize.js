let Rule = require('./rule').model.Rule;

class RuleAllRowsAreTheSameSize extends Rule {

    isMet(cells) {
    	if (cells !== undefined) {
            let sizeRow = cells[0].length;
    	    cells.forEach(function(row) {
                if (sizeRow !== row.length) {
                    sizeRow = -1;
                }
            });
            return (sizeRow !== -1);
    	}
    	return false;
    }
}

module.exports.model = {};
module.exports.model.RuleAllRowsAreTheSameSize = RuleAllRowsAreTheSameSize;
