let CellPassable = require('./cellPassable').model.CellPassable;
let CellImpassable = require('./cellImpassable').model.CellImpassable;

class CellsContainer extends Array {

    getCellsPassablesConnectedWith(row,col) {
        let cells = [];
        // up
        let pos = {row: row - 1, col: col};
        let cell = this.getCellWithPosition(pos.row,pos.col);
        if ((cell !== null) && cell.isPassable()) {
            cells.push(cell);
        }
        // down
        pos = {row: row + 1, col: col};
        cell = this.getCellWithPosition(pos.row,pos.col);
        if ((cell !== null) && cell.isPassable()) {
            cells.push(cell);
        }
        // left
        pos = {row: row, col: col - 1};
        cell = this.getCellWithPosition(pos.row,pos.col);
        if ((cell !== null) && cell.isPassable()) {
            cells.push(cell);
        }
        // right
        pos = {row: row, col: col + 1};
        cell = this.getCellWithPosition(pos.row,pos.col);
        if ((cell !== null) && cell.isPassable()) {
            cells.push(cell);
        }
        return cells;
    }

    getCellWithPosition(row,col) {
        if (this[row] !== undefined) {
            if (this[row][col] !== undefined) {
                return this[row][col];
            }
        }
        return null;
    }

    getAllCellsPassables() {
        let cellsPassables = [];
        this.forEach(function(row) {
            row.forEach(function(cell) {
                if (cell.isPassable()) {
                    cellsPassables.push(cell);
                }
            })
        });
        return cellsPassables;
    }

    addCellPassable(row,col) {
        let cell = new CellPassable(row,col);
        this[row] = this[row] || [];
        this[row][col] = cell;
    }
    addCellImpassable(row,col) {
        let cell = new CellImpassable(row,col);
        this[row] = this[row] || [];
        this[row][col] = cell;
    }
}

module.exports.model = {};
module.exports.model.CellsContainer = CellsContainer;