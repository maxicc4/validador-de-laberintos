let CellsContainer = require('./cellsContainer').model.CellsContainer;
let RuleAllRowsAreTheSameSize = require('./ruleAllRowsAreTheSameSize').model.RuleAllRowsAreTheSameSize;
let RuleAtLeastTwoPassableCells = require('./ruleAtLeastTwoPassableCells').model.RuleAtLeastTwoPassableCells;
let RuleImpassableEdges = require('./ruleImpassableEdges').model.RuleImpassableEdges;
let RuleAllCellsAreConnected = require('./ruleAllCellsAreConnected').model.RuleAllCellsAreConnected;
let RuleAllPassableCellsMustBeConnectedWithAtLeastTwoCellsPassable = require('./ruleAllPassableCellsMustBeConnectedWithAtLeastTwoCellsPassable').model.RuleAllPassableCellsMustBeConnectedWithAtLeastTwoCellsPassable;
let RuleThereCannotBeFourCellsAttached = require('./ruleThereCannotBeFourCellsAttached').model.RuleThereCannotBeFourCellsAttached;

class Maze {
    constructor(cells, impassableTypes) {
    	this.cells = new CellsContainer();
    	for (let row = 0; row < cells.length; row++) {
    		for (let col = 0; col < cells[row].length; col++) {
    			if (impassableTypes.includes(cells[row][col])) {
    				this.cells.addCellImpassable(row,col);
    			} else {
    				this.cells.addCellPassable(row,col);
    			}
    		}
    	}
    	this.rules = [
    		new RuleAllRowsAreTheSameSize(),
    		new RuleAtLeastTwoPassableCells(),
    		new RuleImpassableEdges(),
    		new RuleAllCellsAreConnected(),
    		new RuleAllPassableCellsMustBeConnectedWithAtLeastTwoCellsPassable(),
    		new RuleThereCannotBeFourCellsAttached()
    	];
    }

    checkIfIsValid() {
    	for (let i = 0; i < this.rules.length; i++) {
    		if (!this.rules[i].isMet(this.cells)) {
    			return {isValid: false, message: this.getMessageInvalid(this.rules[i])};
    		}
    	}
    	return {isValid: true};
    }

    getMessageInvalid(rule) {
    	return ('Invalid' + rule.constructor.name.replace(/([A-Z])/g, ' $1').replace(/Rule/g, 'Rule:'));
	}
}

module.exports.model = {};
module.exports.model.Maze = Maze;
