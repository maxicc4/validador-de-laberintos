let Cell = require('./cell').model.Cell;

class CellImpassable extends Cell {

    isPassable() {
        return false;
    }
}

module.exports.model = {};
module.exports.model.CellImpassable = CellImpassable;