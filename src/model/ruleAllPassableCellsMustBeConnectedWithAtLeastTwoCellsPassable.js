let Rule = require('./rule').model.Rule;

class RuleAllPassableCellsMustBeConnectedWithAtLeastTwoCellsPassable extends Rule {

    isMet(cells) {
    	if (cells !== undefined) {
    	    for (let row = 0; row < cells.length; row++) {
    	        for (let col = 0; col < cells[row].length; col++) {
    	        	if (cells[row][col].isPassable()) {
                        if (cells.getCellsPassablesConnectedWith(row, col).length < 2) {
                        	return false;
						}
                    }
                }
            }
            return true;
    	}
    	return false;
    }
}

module.exports.model = {};
module.exports.model.RuleAllPassableCellsMustBeConnectedWithAtLeastTwoCellsPassable = RuleAllPassableCellsMustBeConnectedWithAtLeastTwoCellsPassable;
