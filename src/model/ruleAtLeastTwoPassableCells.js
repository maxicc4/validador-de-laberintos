let Rule = require('./rule').model.Rule;

class RuleAtLeastTwoPassableCells extends Rule {

    isMet(cells) {
        let numCellsPassables = 0;
    	if (cells !== undefined) {
            cells.forEach(function(row) {
                row.forEach(function(cell) {
                    if (cell.isPassable()) {
                        numCellsPassables++;
                    }
                })
            });
            return (numCellsPassables >= 2);
    	}
    	return false;
    }
}

module.exports.model = {};
module.exports.model.RuleAtLeastTwoPassableCells = RuleAtLeastTwoPassableCells;
