let Rule = require('./rule').model.Rule;

class RuleImpassableEdges extends Rule {

    isMet(cells) {
    	if (cells !== undefined) {
    	    // Top edge
            for (let i = 0; i < cells[0].length; i++) {
                if (cells[0][i].isPassable()) {
                    return false;
                }
            }
            // Left and right edges
            for (let i = 1; i < cells.length; i++) {
                let sizeRow = cells[i].length;
                if (cells[i][0].isPassable() || cells[i][sizeRow-1].isPassable()) {
                    return false;
                }
            }
            // Bottom edge
            for (let i = 0; i < cells[cells.length-1].length; i++) {
                if (cells[cells.length-1][i].isPassable()) {
                    return false;
                }
            }
            return true;
    	}
    	return false;
    }
}

module.exports.model = {};
module.exports.model.RuleImpassableEdges = RuleImpassableEdges;
