let Cell = require('./cell').model.Cell;

class CellPassable extends Cell {

    isPassable() {
        return true;
    }
}

module.exports.model = {};
module.exports.model.CellPassable = CellPassable;