let Rule = require('./rule').model.Rule;

class RuleThereCannotBeFourCellsAttached extends Rule {

    isMet(cells) {
    	if (cells !== undefined) {
    	    let cellsPassables = cells.getAllCellsPassables();
    	    let currentCell;
    	    let cellsConnectedDirectly;
    	    let cellsConnected;

    	    while (cellsPassables.length > 0) {
    	    	currentCell = cellsPassables.shift();

    	    	cellsConnectedDirectly = cells.getCellsPassablesConnectedWith(currentCell.row,currentCell.col);
    	    	cellsConnected = [];
    	    	for (let i = 0; i < cellsConnectedDirectly.length; i++) {
    	    		cellsConnected = cellsConnected.concat(cells.getCellsPassablesConnectedWith(cellsConnectedDirectly[i].row,cellsConnectedDirectly[i].col));
    	    	}
    	    	cellsConnected = this.filterMapReduce(cellsConnected,currentCell);
                for (let property in cellsConnected) {
                    if (cellsConnected.hasOwnProperty(property)) {
                        if (cellsConnected[property] >= 2) {
                        	return false;
						}
                    }
                }
    	    }
    	    return true;
    	}
    	return false;
    }

    filterMapReduce(cellsConnected, currentCell) {
    	// Saco la celda actual
        cellsConnected = cellsConnected.filter(value => value !== currentCell);
        // Las transformo a 'row,col'
        cellsConnected = cellsConnected.map(value => (value.row.toString() + ',' + value.col.toString()));
        // Voy agrupando y contando
        cellsConnected = cellsConnected.reduce(function(groups, value) {
            groups[value] = groups[value] || 0;
            groups[value]++;
            return groups;
        }, {});
        return cellsConnected;
	}
}

module.exports.model = {};
module.exports.model.RuleThereCannotBeFourCellsAttached = RuleThereCannotBeFourCellsAttached;
