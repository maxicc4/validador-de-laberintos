class Cell {

    constructor(row,col) {
        this.row = row;
        this.col = col;
    }

    isPassable() {
        throw new Error('You have to implement the method isPassable!');
    }
}

module.exports.model = {};
module.exports.model.Cell = Cell;