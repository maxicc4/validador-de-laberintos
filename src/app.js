
/**
 * Expose `createApplication()`.
 */
exports = module.exports = createApplication;

function createApplication(path, port) {
    let express = require('express');
    let bodyParser = require('body-parser');

    let app = express();

    // parse application/json
    app.use(bodyParser.json());

    let Maze = require('./model/maze').model.Maze;

    app.post(path, function (req, res) {
        if (req.body.hasOwnProperty("cells") && req.body.hasOwnProperty("impassableTypes")) {
            let maze = new Maze(req.body.cells, req.body.impassableTypes);
            let valid = maze.checkIfIsValid();
            if (valid.isValid) {
                res.status(200).json({});
            } else {
                res.status(422).json({message: valid.message});
            }
        } else {
            res.status(400).json({message: "Bad Request"});
        }
    });

    app.listen(port, function () {
    });
}