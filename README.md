# Validador de Laberintos

Trabajo práctico para la materia Técnicas de diseño (75.10) FIUBA - 1°C 2019

## Comandos de instalación

### Si lo corremos mediante docker
Requisitos: docker

- ```docker build -t valid-laberintos-app .```
- ```docker run --rm -p3000:3000 --name valid-lab-run-app valid-laberintos-app```

### Si lo corremos localmente
Requisitos: nodejs 10, npm

- ```npm install```
- ```npm start```

De cualquiera de las dos formas, queda la aplicación corriendo y escuchando el puerto 3000.

## Uso
Una vez instalada y corriendo, la aplicacion está esperando que se le pase por POST la información necesaria en formato JSON.

### Ejemplos de uso
#### Caso exitoso
Request:
```
POST​ /v1.0/maze
{
	"cells": [
		[0, 0, 0, 0, 0, 0, 0],
		[0, 1, 1, 1, 1, 1, 0],
		[0, 1, 2, 1, 2, 1, 0],
		[0, 1, 1, 1, 1, 1, 0],
		[0, 0, 0, 0, 0, 0, 0]
	],
	"impassableTypes": [
		0,
		2
	]
}
```
Response:
```
HTTP Code 200 Success
{
}
```
#### Caso en el que falla una regla
Request:
```
POST​ /v1.0/maze
{
	"cells": [
		[0, 0, 0, 0, 0, 0, 0],
		[0, 1, 1, 1, 1, 1, 0],
		[0, 1, 2, 1, 2, 1, 0],
		[0, 1, 1, 1, 1, 1, 0],
		[0, 0, 0, 0, 0, 0, 1]
	],
	"impassableTypes": [
		0,
		2
	]
}
```
Response:
```
HTTP Code 422 Unprocessable Entity
{
    "message" : "Invalid Rule: Impassable Edges"
}
```
#### Caso en el que no se le pasan las celdas o los tipos intransitables
Request:
```
POST​ /v1.0/maze
{
	"cells": [
		[0, 0, 0, 0, 0, 0, 0],
		[0, 1, 1, 1, 1, 1, 0],
		[0, 1, 2, 1, 2, 1, 0],
		[0, 1, 1, 1, 1, 1, 0],
		[0, 0, 0, 0, 0, 0, 0]
	]
}
```
Response:
```
HTTP Code 400 Bad Request
{
    "message" : "Bad Request"
}
```

## Desarrollo
### Reglas
Para el desarrollo se tuvieron en cuenta las siguientes reglas:
- El laberinto tiene que tener al menos dos celdas transitables
- Toda celda transitable debe estar conectada al menos a otras dos celdas transitables
- Existe al menos un camino de una celda a otra (transitables)
- No pueden haber cuatro celdas pegadas
- Los bordes tienen que ser intransitables
- Todas las filas tienen que tener el mismo tamaño

Se consideró que se podía analizar si el laberinto cumplía cada regla por separado (no interfería una con la otra), 
entonces se creó una clase particular para cada regla.

### Diagrama de clases (esqueleto)
![alt text](diag_clases_esqueleto.png "Diagrama de clases (esqueleto)")

### Tests
Como parte del desarrollo, se hicieron una serie de pruebas unitarias que garantizaron un mínimo de funcionamiento del modelo.