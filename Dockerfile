FROM node:10

#RUN mkdir /app

WORKDIR /app

COPY src ./src
COPY test ./test
COPY package.json .
COPY package-lock.json .
COPY README.md .

RUN npm install
RUN ls /
RUN ls /app

EXPOSE 3000

CMD ["npm","start"]